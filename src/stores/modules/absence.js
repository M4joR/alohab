import axios from "axios";

export default {
    state: {
        absence: []
    },
    mutations: {
        setAbsence(state, absence) {
            state.absence = absence;
        },
    },
    actions: {
        ADD_ABSENCE:(context, payload) =>{
             return new Promise((resolve, reject) =>{
                axios.post(process.env.VUE_APP_BACKEND_URL+'absence', payload).then(() =>{
                    resolve()
                }).catch(err => reject(err))
            })
        },
        FETCH_ALL_ABSENCE:(context) =>{
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL + 'absence').then(response =>{
                    context.commit('setAbsence', response.data)
                    resolve(response.data)
                }).catch(err => reject(err))
            })
        }
    },
    getters: {
        absence: state => state.absence,
    },
}