import axios from "axios";

export default {
    state: {
        deletedUsers: [],
        users: [],
        actualUsers: [],
        allDeletedUsers:[],
    },
    mutations: {
        setUsers(state, user) {
            state.users = user;
        },

        setDeletedUsers(state, deletedUsers) {
            state.deletedUsers = deletedUsers;
        },

        setActualUsers(state, actualUsers) {
            state.actualUsers = actualUsers;
        },

        setAllDeletedUsers(state, allDeletedUsers){
            state.allDeletedUsers = allDeletedUsers
        }

    },
    actions: {
        FETCH_USERS: (context, clanIds) => {
            return new Promise((resolve, reject) => {
                axios.get('https://api.worldoftanks.eu/wot/clans/info/?application_id='+process.env.VUE_APP_APPLICATION_ID+'&clan_id=' + clanIds)
                    .then(response => {
                        let users = [];
                        clanIds.forEach(clanId => {
                            response.data.data[clanId].members.forEach(user => {
                                users.push({
                                    userId: user.account_id,
                                    clanId: clanId,
                                    userName: user.account_name
                                })
                            })
                        })
                        context.commit('setUsers', users)
                        resolve(users)
                    }).catch(err => reject(err))
            })
        },

        FETCH_DELETED_USERS: (context, payload) => {
            return new Promise((resolve, reject) => {

                axios.post(process.env.VUE_APP_BACKEND_URL+'userWithClan/deletedUsers', payload)
                    .then(response => {
                        console.log(response.data)
                        context.commit('setDeletedUsers', response.data)
                        resolve()
                    }).catch(err => reject(err))

            })
        },

        FETCH_CLAN_MEMBERS: (context, clanId) => {
            return new Promise((resolve, reject) => {
                axios.get('https://api.worldoftanks.eu/wot/clans/info/?application_id='+process.env.VUE_APP_APPLICATION_ID+'&clan_id=' + clanId +'&language=pl')
                    .then(response => {
                        let clanMembers = []
                        console.log(Object.values(response.data.data))
                        response.data.data[clanId].members.forEach(user => {
                            clanMembers.push({
                                clanId: clanId,
                                userId: user.account_id,
                                userName: user.account_name,
                                userRoleName: user.role_i18n,
                                userRole: user.role
                            })
                        })
                        context.commit('setActualUsers', clanMembers)
                        resolve(clanMembers)
                    }).catch(err => reject(err))
            })
        },

        ADD_CLAN_MEMBERS: (context, payload) =>{
            return new Promise((resolve, reject) => {
                axios.post(process.env.VUE_APP_BACKEND_URL+'userWithClan/addUserWithClan', payload).then(() =>{
                    resolve()
                }).catch(err => reject(err))
            })
        },
        FETCH_ALL_DELETED_USERS: (context) =>{
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+'deletedUsers/allDeletedUsers').then(response =>{
                    context.commit('setAllDeletedUsers', response.data)
                    resolve(response.data)
                }).catch(err => reject(err))
            })
        },
        DELETE_USERS_BY_CLAN_ID:(context, payload) =>{
            return new Promise((resolve, reject) => {
                axios.delete(process.env.VUE_APP_BACKEND_URL+'userWithClan/deleteUsers/' + payload).then(()=> resolve())
                    .catch(err =>reject(err))
            })
        },

        ACCEPT_DELETE_USER_BY_USER_ID:(context, payload) =>{
            return new Promise((resolve, reject) => {
                console.log(payload.owner)
                axios.delete(process.env.VUE_APP_BACKEND_URL+'deletedUsers/accept/' + payload.userId+"/"+payload.owner).then(()=> resolve())
                    .catch(err =>reject(err))
            })
        },

        REJECT_DELETE_USER_BY_USER_ID:(context, payload) =>{
            return new Promise((resolve, reject) => {
                axios.delete(process.env.VUE_APP_BACKEND_URL+'deletedUsers/reject/' + payload.userId+"/"+payload.owner).then(()=> resolve())
                    .catch(err =>reject(err))
            })
        }
    },
    getters: {
        users: state => state.users,
        deletedUsers: state => state.deletedUsers,
        actualUsers: state => state.actualUsers,
        allDeletedUsers: state => state.allDeletedUsers,
    },
}