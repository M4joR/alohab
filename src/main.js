import Vue from 'vue'
import VueCookies from 'vue-cookies'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import router from './router'
import VueRouter from 'vue-router'
import store from "@/stores/store";
import './styles.scss'
import { LMap, LTileLayer, LMarker} from 'vue2-leaflet'
import { Icon } from 'leaflet'

Vue.config.productionTip = false
Vue.use(IconsPlugin)
Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.use(VueCookies)
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
