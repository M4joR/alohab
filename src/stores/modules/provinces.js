import axios from "axios";

export default {
    state: {
        clanProvinces: [],
        clanBattles: [],
        clanDefends: [],
    },
    mutations: {
        setClanBattles(state, clanBattles){
            state.clanBattles = clanBattles;
        },
        setClanProvinces(state, clanProvinces){
            state.clanProvinces =clanProvinces;
        },
        setClanDefends(state, clanDefends){
            state.clanDefends =clanDefends;
        }
    },
    actions: {
        FETCH_CLAN_BATTLES: (context) => {
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+'provinces/clanBattles')
                    .then(response => {
                        context.commit('setClanBattles', response.data)
                        resolve(response.data)
                    }).catch(err => reject(err))
            })
        },
        FETCH_CLAN_PROVINCES: (context) =>{
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+'provinces')
                    .then(response => {
                        context.commit('setClanProvinces', response.data)
                        resolve(response.data)
                    }).catch(err => reject(err))
            })
        },
        FETCH_CLAN_DEFENDS: (context) =>{
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+'provinces/clanDefends')
                    .then(response => {
                        context.commit('setClanDefends', response.data)
                        resolve(response.data)
                    }).catch(err => reject(err))
            })
        }
    },
    getters: {
        clanBattles: state => state.clanBattles,
        clanProvinces : state => state.clanProvinces,
        clanDefends : state => state.clanDefends
    },
}