import Vuex from 'vuex'
import Vue from 'vue'
import Users from "@/stores/modules/users";
import Clans from "@/stores/modules/clans";
import Tanks from "@/stores/modules/tanks";
import Time from '@/stores/modules/time';
import Notifications from '@/stores/modules/notifications';
import Statistic from '@/stores/modules/statistic';
import Maps from '@/stores/modules/maps';
import Provinces from '@/stores/modules/provinces';
import Absence from '@/stores/modules/absence';
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        users: Users,
        clans: Clans,
        tanks: Tanks,
        time: Time,
        notifications: Notifications,
        statistic : Statistic,
        maps: Maps,
        provinces: Provinces,
        absence: Absence,
    },
})