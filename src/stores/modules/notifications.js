import axios from "axios";

export default {
    state: {
        deletedUserNotifications: [],
    },
    mutations: {
        setDeletedUsersNotification(state, deletedUserNotifications) {
            state.deletedUserNotifications = deletedUserNotifications;
        },
    },
    actions: {
        FETCH_DELETED_USERS_NOTIFICATIONS: (context) => {
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+'notifications')
                    .then(response => {
                        context.commit('setDeletedUsersNotification', response.data)
                        resolve(response.data)
                    }).catch(err => reject(err))
            })
        },

    },
    getters: {
        deletedUserNotifications: state => state.deletedUserNotifications,
    },
}