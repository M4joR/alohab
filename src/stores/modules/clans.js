import axios from "axios";

export default {
    state: {
        clans: [],
        foundedClans: []
    },
    mutations: {
        setClans(state, clan) {
            state.clans = clan;
        },

        setFoundedClans(state, foundedClan) {
            state.foundedClans = foundedClan;
        },
    },
    actions: {
        FETCH_CLANS: (context) => {
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL + 'clan')
                    .then(response => {
                        context.commit('setClans', response.data)
                        resolve(response.data)
                    }).catch(err => reject(err))
            })
        },

        FETCH_FOUNDED_CLANS: (context, clanName) => {
            return new Promise((resolve, reject) => {
                axios.get('https://api.worldoftanks.eu/wot/clans/list/?application_id='+process.env.VUE_APP_APPLICATION_ID+'&search=' + clanName)
                    .then(response =>{
                        let clanList = response.data.data.map(el => {
                            return {
                                clanId: el.clan_id,
                                clanName: el.name,
                                clanEmblem: el.emblems.x64.portal,
                                clanTag: el.tag
                            }
                        })
                        context.commit('setFoundedClans', clanList)
                        resolve(clanList)
                    }).catch(err => reject(err))

            })
        },

        ADD_CLAN:(context, clan) =>{
            return new Promise((resolve, reject) => {
                axios.put(process.env.VUE_APP_BACKEND_URL+'clan/addClan', clan).then(() => {
                   resolve()
                }).catch(err => reject(err))
            })
        },

        DELETE_CLAN:(context, clanId) =>{
            return new Promise((resolve, reject) => {
                axios.delete(process.env.VUE_APP_BACKEND_URL+'clan/deleteClan/' + clanId).then(() => {
                   resolve()
                }).catch(err => reject(err))
            })
        },
    },
    
    getters: {
        clans: state => state.clans,
        foundedClans: state => state.foundedClans
    },
}