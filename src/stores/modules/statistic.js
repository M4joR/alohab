import axios from "axios";

export default {
    state: {
        clanStatistic: [],
    },
    mutations: {
        setClanStatistic(state, clanStatistic) {
            state.clanStatistic = clanStatistic;
        },
    },
    actions: {
        FETCH_CLAN_STATISTIC: (context) => {
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+"/basicClan")
                    .then(response => {
                        let clanRating = response.data
                        context.commit('setClanStatistic', clanRating)
                        resolve(clanRating)
                    }).catch(err => reject(err))
            })
        },
    },
    getters: {
        clanStatistic: state => state.clanStatistic,
    },
}