export default {
    state: {
        tacticMap: 'https://nouri.blue/public/mines',
        tacticTank: 'https://wotstats.org/images/game/mediumTank.png'
    },
    mutations: {
        setTacticMap(state, tacticMap) {
            state.tacticMap = tacticMap;
        },
        setTacticTank(state, tacticTank) {
            state.tacticTank = tacticTank;
        },
    },
    actions: {
        SET_ACTUAL_MAP: (context, mapUrl) => {
            context.commit('setTacticMap', mapUrl)
        },

        SET_ACTUAL_TANK: (context, tacticTank) => {
            context.commit('setTacticTank', tacticTank)
        },

    },
    getters: {
        tacticMap: state => state.tacticMap,
        tacticTank: state => state.tacticTank
    },
}