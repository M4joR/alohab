import axios from "axios";

export default {
    state: {
        tanks: [],
    },
    mutations: {
        setTanks(state, tanks) {
            state.tanks = tanks;
        },
    },
    actions: {
        FETCH_TANKS: (context) => {
            return new Promise((resolve, reject) => {
                axios.get('https://api.worldoftanks.eu/wot/encyclopedia/vehicles/?application_id='+process.env.VUE_APP_APPLICATION_ID+'&fields=name%2C+tank_id%2C+images.contour_icon&tier=10&language=pl')
                    .then(response => {
                        let tanks = response.data.data
                        context.commit('setTanks', tanks)
                        resolve(tanks)
                    }).catch(err => reject(err))
            })
        },
    },
    getters: {
        tanks: state => state.tanks,
    },
}