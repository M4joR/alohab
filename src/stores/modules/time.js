import axios from "axios";

export default {
    state: {
        time: null,
    },
    mutations: {
        setTimes(state, time) {
            state.time = time;
        },
    },
    actions: {
        FETCH_TIME: (context) => {
            return new Promise((resolve, reject) => {
                axios.get(process.env.VUE_APP_BACKEND_URL+'refreshTime')
                    .then(response => {
                        let timeMap = new Map();
                        response.data.map(time=>{
                            timeMap.set(time.refreshName, time.refreshDate + " " + time.refreshTime)
                        })
                        context.commit('setTimes', timeMap)
                        resolve(timeMap)
                    }).catch(err => reject(err))
            })
        },

    },
    getters: {
        time: state => state.time,
    },
}