import VueRouter from "vue-router"
import Clans from "@/components/findNewPeople/Clans";
import DeletedUsers from "@/components/findNewPeople/DeletedUsers";
import DeletedUserNotifications from "@/components/findNewPeople/DeletedUserNotifications";
import NotLoggedErrorPage from "@/components/errorPage/NotLoggedErrorPage";
import VueCookies from "vue-cookies"
import jwtDecode from "jwt-decode";
import NotInClanErrorPage from "@/components/errorPage/NotInClanErrorPage";
import NotPermissionErrorPage from "@/components/errorPage/NotPermissionErrorPage";
import Home from "@/components/Home";
import TacticRoom from "@/components/tactic/TacticRoom";
import Battles from "@/components/globalMap/Battles";
import ClanProvinces from "@/components/globalMap/ClanProvinces";
import AddAbsence from "@/components/absence/AddAbsence";
import ShowAllAbsences from "@/components/absence/ShowAllAbsences";

const router =  new VueRouter({
    mode: "history",
    routes: [
        {
            name: "clans",
            path: '/clans',
            component: Clans
        },
        {
            name: "deletedUsers",
            path: '/deletedUsers',
            component: DeletedUsers
        },
        {
            name: "notifications",
            path: '/notifications',
            component: DeletedUserNotifications
        },
        {
            name: "notLoggedErrorPage",
            path: "/notLoggedErrorPage",
            component: NotLoggedErrorPage
        },
        {
            name: "notInClanErrorPage",
            path: "/notInClanErrorPage",
            component: NotInClanErrorPage
        },
        {
            name: "notPermissionErrorPage",
            path: "/notPermissionErrorPage",
            component: NotPermissionErrorPage
        },
        {
            name: "tacticRoom",
            path: "/tacticRoom",
            component: TacticRoom
        },
        {
            name: "home",
            path: "/",
            component: Home
        },
        {
            name: "battles",
            path: "/battles",
            component: Battles
        },
        {
            name: "provinces",
            path: "/clanProvinces",
            component: ClanProvinces
        },
        {
            name: "addAbsence",
            path: "/addAbsence",
            component: AddAbsence
        }
        ,
        {
            name: "absences",
            path: "/absence",
            component: ShowAllAbsences
        }
    ]
})

router.beforeEach((to, from, next) => {
    if(to.name !== "notLoggedErrorPage" && VueCookies.get('ALOHA_TOKEN') === null) {
        next("/notLoggedErrorPage")
    }else if(VueCookies.get('ALOHA_TOKEN') !== null){
        let token = jwtDecode(VueCookies.get('ALOHA_TOKEN'))
        if(token.userRole === "notInClan" && to.name !== "notInClanErrorPage"){
            next("/notInClanErrorPage")
        }else{
            if(token.accountId === 558175399){
                next()
            }else if((to.name === "deletedUsers" || (to.name === "clans" )) && token.userRoleId < 7 ){
                next("/notPermissionErrorPage")
            }else{
                next()
            }
        }
    }else{
        next()
    }
})

export default router